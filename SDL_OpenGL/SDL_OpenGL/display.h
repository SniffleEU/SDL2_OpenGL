#pragma once

#include <string>
#include <SDL\SDL.h>

class display
{
public:
	bool gIsclosedf();
	void clear();
	display(int width, int height, const std::string &title);
	virtual ~display();
	void update();

	inline int getWidth(){ return SDL_GetWindowSurface(gWindow)->w;}
	inline int getHeight(){ return SDL_GetWindowSurface(gWindow)->h; }

private:
	SDL_Window* gWindow;
	SDL_GLContext gGlContext;
	bool gIsclosed;
};

