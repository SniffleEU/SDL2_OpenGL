#include "display.h"
#include <GL/glew.h>

bool display::gIsclosedf()
{
	return gIsclosed;
}

void display::clear()
{
	glClearColor(0.0f, 0.15f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

display::display(int width, int height, const std::string &title)
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	//Set opengl attributes
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	//Create window
	gWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);

	gGlContext = SDL_GL_CreateContext(gWindow);

	SDL_GL_SetSwapInterval(1);

	GLenum status = glewInit();

	gIsclosed = false;

	glEnable(GL_DEPTH_TEST);

}


void display::update() 
{
	SDL_GL_SwapWindow(gWindow);

	//Event handler
	SDL_Event e;

	//Event poll loop
	while (SDL_PollEvent(&e) != 0)
	{
		//Exit window
		if (e.type == SDL_QUIT)
		{
			gIsclosed = true;
		}
	}

}

display::~display()
{
	SDL_GL_DeleteContext(gGlContext);
	SDL_DestroyWindow(gWindow);
	SDL_Quit();
}
