#include "Shader.h"
#include <iostream>
#include <fstream>


Shader::Shader(const std::string &fileName)
{
	gProgram = glCreateProgram();

	gShaders[0] = CreateShader(LoadShader(fileName + ".vsh") , GL_VERTEX_SHADER);
	gShaders[1] = CreateShader(LoadShader(fileName + ".fsh"), GL_FRAGMENT_SHADER);

	for (unsigned int i = 0; i < NUM_SHADERS; i++)
		glAttachShader(gProgram, gShaders[i]);
	
	glBindAttribLocation(gProgram, 0, "position");
	glBindAttribLocation(gProgram, 1, "texCoord");
	glBindAttribLocation(gProgram, 2, "normal");

	glLinkProgram(gProgram);
	CheckShaderError(gProgram, GL_LINK_STATUS, true, "Error: Program linking failed: ");

	glValidateProgram(gProgram);
	CheckShaderError(gProgram, GL_VALIDATE_STATUS, true, "Error: Program is invalid: ");

	gUniforms[TRANSFORM_U] = glGetUniformLocation(gProgram, "transform");

}

void Shader::bind()
{
	glUseProgram(gProgram);
}

void Shader::update(const Transform & transform, const Camera& camera)
{
	glm::mat4 Model = camera.getViewProject() * transform.getModel();

	glUniformMatrix4fv(gUniforms[TRANSFORM_U], 1,GL_FALSE, &Model[0][0]);
}

GLuint Shader::CreateShader(const std::string& text, unsigned int type)
{
  GLuint shader = glCreateShader(type);

    if(shader == 0)
		std::cerr << "Error compiling shader type " << type << std::endl;

    const GLchar* p[1];
    p[0] = text.c_str();
    GLint lengths[1];
    lengths[0] = text.length();

    glShaderSource(shader, 1, p, lengths);
    glCompileShader(shader);

    CheckShaderError(shader, GL_COMPILE_STATUS, false, "Error compiling shader!");

return shader;
}

Shader::~Shader()
{
	for (unsigned int i = 0; i < NUM_SHADERS; i++)
	{
		glDetachShader(gProgram, gShaders[i]);
		glDeleteShader(gShaders[i]);
	}
	glDeleteProgram(gProgram);
}


std::string Shader::LoadShader(const std::string& fileName)
{
	std::ifstream file;
	file.open((fileName).c_str());

	std::string output;
	std::string line;

	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
	{
		std::cerr << "Unable to load shader: " << fileName << std::endl;
	}

	return output;
}

void Shader::CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage)
{
	GLint success = 0;
	GLchar error[1024] = { 0 };

	if (isProgram)
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);

	if (success == GL_FALSE)
	{
		if (isProgram)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);

		std::cerr << errorMessage << ": '" << error << "'" << std::endl;
	}
}


