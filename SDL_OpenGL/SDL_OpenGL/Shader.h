#pragma once
#include <string>
#include <GL/glew.h>
#include "Transform.h"
#include "Camera.h"

class Shader
{
public:
	Shader(const std::string &fileName);

	void bind();

	void update(const Transform& transform, const Camera& camera);

	virtual ~Shader();

private:
	static const unsigned int NUM_SHADERS = 2;
	enum {

		TRANSFORM_U,


		NUM_UNIFORMS

	};
	GLuint gProgram;
	GLuint gShaders[NUM_SHADERS];
	GLuint gUniforms[NUM_UNIFORMS];



	std::string LoadShader(const std::string& fileName);
	void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage);

	GLuint CreateShader(const std::string& text, unsigned int type);
};

