#pragma once
#include <glm\glm.hpp>
#include <GL\glew.h>
#include "obj_loader.h"

class vertex {
public:
	vertex(const glm::vec3& pos, const glm::vec2& texcoord, const glm::vec3& normal = glm::vec3(0,0,0))
	{
		this->pos = pos;
		this->texCoord = texcoord;
		this->normal = normal;
	}

	inline glm::vec3* getPos() { return &pos; }
	inline glm::vec3* getNormal() { return &normal; }
	inline glm::vec2* gettexCoord() { return &texCoord; }
protected:

private:
	glm::vec3 pos;
	glm::vec2 texCoord;
	glm::vec3 normal;

};

class Mesh
{
public:
	void draw();
	Mesh(vertex* vertices, unsigned int numvertices, unsigned int* indices, unsigned int numIndices);
	Mesh(const std::string& filename);
	virtual ~Mesh();

private:

	enum 
	{
		POSITION_VB,
		TEXCOORD_VB,
		NORMAL_VB,
		INDEX_VB,

		NUM_BUFFERS
	};

	GLuint gVertexArrayObject;
	GLuint gVertexArrayBuffers[NUM_BUFFERS];
	unsigned int gDrawCount;

	void initMesh(const IndexedModel& model);

};

