#pragma once
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>
class Transform
{
public:
	Transform(const glm::vec3& pos = glm::vec3(), const glm::vec3& rot = glm::vec3(),const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)) : 
		gPos(pos), 
		gRot(rot),
		gScale(scale)
	{}

	inline glm::mat4 getModel() const
	{
		glm::mat4 posMatrix = glm::translate(gPos);
		glm::mat4 rotXMatrix = glm::rotate(gRot.x, glm::vec3(1, 0, 0));
		glm::mat4 rotYMatrix = glm::rotate(gRot.y, glm::vec3(0, 1, 0));
		glm::mat4 rotZMatrix = glm::rotate(gRot.z, glm::vec3(0, 0, 1));
		glm::mat4 scaleMatrix = glm::scale(gScale);

		glm::mat4 rotMatrix = rotZMatrix * rotYMatrix * rotXMatrix;

		return posMatrix * rotMatrix * scaleMatrix;

	};

	~Transform() {};

	inline glm::vec3& GetPos() { return gPos; }
	inline glm::vec3& GetRot() { return gRot; }
	inline glm::vec3& GetScale() { return gScale; }

private:
	glm::vec3 gPos;
	glm::vec3 gRot;
	glm::vec3 gScale;
};

