#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
class Camera
{
public:
	Camera(const glm::vec3& pos, float fov, float aspect, float zNear, float zFar) 
	{
		gPerspective = glm::perspective(fov, aspect, zNear, zFar);
		gPosition = pos;

		gForward = glm::vec3(0.0, 0.0, 1.0);
		gUp = glm::vec3(0.0, 1.0, 0.0);

	};

	inline glm::mat4 getViewProject() const
	{
		return gPerspective * glm::lookAt(gPosition, gPosition + gForward, gUp);
	}

	inline glm::vec3& GetForward() { return gForward; }
	inline glm::vec3& GetUp() { return gUp; }

	~Camera() {};
private:
	glm::mat4 gPerspective;
	glm::vec3 gPosition;
	glm::vec3 gForward;
	glm::vec3 gUp;
};

