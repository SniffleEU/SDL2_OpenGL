#pragma once
#include <string>
#include <SDL/SDL.h>
#include <SDL_IMAGE/SDL_image.h>
#include <GL\glew.h>
#include <cassert>
//#include <SDL_ttf.h>

class LTexture
{
public:
	//Initialize
	LTexture(std::string path);
	//Release object
	~LTexture();
	//Release texture 
	void free();

	void bind(unsigned int unit);


private:
	//The actual hardware texture
	GLuint gTexture;

};

