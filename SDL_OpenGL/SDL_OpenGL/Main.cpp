#include <iostream>
#include <GL/glew.h>
#include <SDL_IMAGE/SDL_image.h>

#include "display.h"
#include "Shader.h"
#include "Mesh.h"
#include "lTexture.h"
#include "Transform.h"
#include "Camera.h"


int main(int argc, char* args[])
{
	display gDisplay(800, 600, "SDL_OpenGL");
	Shader shader("./res/basicShader");
	vertex vertices[] = { vertex(glm::vec3(-0.5, -0.5, 0), glm::vec2(0.0,0.0)),
						  vertex(glm::vec3(0, 0.5, 0), glm::vec2(0.5,1.0)),
						  vertex(glm::vec3(0.5, -0.5, 0), glm::vec2(1.0, 0.0)) };

	unsigned int indices[] = { 0,1,2 };

	Mesh mesh(vertices, sizeof(vertices) / sizeof(vertices[0]), indices, sizeof(indices)/sizeof(indices[0]));
	Mesh mesh2("./res/monkey3.obj");

	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
	}
	LTexture gTexture = LTexture("./res/bricks.png");

	Camera camera(glm::vec3(0, 0, -4), 70.0f, (float)gDisplay.getWidth() / (float)gDisplay.getHeight(), 0.01f, 1000.0f);

	Transform transform;

	float counter = 0.0f;

	shader.bind();
	gTexture.bind(0);

	//While application is running
	while (!gDisplay.gIsclosedf())
	{
		gDisplay.clear();
		
		transform.GetPos().x = sinf(counter);
		transform.GetRot().z = counter;
		transform.GetRot().y = counter;
		transform.GetRot().x = counter;

		shader.update(transform, camera);

		mesh2.draw();

		gDisplay.update();
		
		counter += 0.01f;
	}

	return 0;
}